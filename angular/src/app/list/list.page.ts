import { legends } from './../names/nameslist';
import { Component } from '@angular/core';


@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage {
  legends: Array<string> = [];

  ngOnInit() {
    this.legends = legends;
  }

}
