import { Component } from '@angular/core';
import { legends } from '../names/nameslist';

@Component({
  selector: 'app-edit',
  templateUrl: 'edit.page.html',
  styleUrls: ['edit.page.scss']
})
export class EditPage {

  legends: Array<string> = [];
  editable: (boolean) = false;

  ngOnInit() {
    this.legends = legends;
  }

  editName(event, label) {
    // contenteditable="true"

    label.color = 'danger';
    console.log(label.getText())

  }

}


