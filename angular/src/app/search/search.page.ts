import { legends } from './../names/nameslist';
import { Component } from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: 'search.page.html',
  styleUrls: ['search.page.scss']
})

export class SearchPage {
  searchInput : string;
  results: Array<string> = [];
  legends: Array<string> = [];

  ngOnInit() {
    this.legends = legends;
  }

  onSearch() {

    if( !this.searchInput ) {
      return this.results = [];
    }

    this.results = legends.filter((legend) => {
      return legend.toLowerCase()
        .indexOf(this.searchInput.toLowerCase()) > -1
    });

  }
}
