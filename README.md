# Folha de respostas

## Javascript Questão 2:

### a) No import da api do google maps no index.html, para que servem as tags async e defer?
R: Quando o nevagador começa a renderizar o HTML e encontra uma tag  `<script>`, acaba parando toda a renderização e dando total atenção para esse recurso, que caso contenha algum conteúdo externo ( `<script src="teste.js">` ), fará o download e executará, só então após disso que continuará a renderização do HTML normalmente. Com isso surgiram os dois atributos chamados `async` e `defer`, para solucionar esse problema de bloqueio de renderização.

`Async`: Indicará ao navegador que o JavaScript será executado assincronamente, ou seja, fará o download paralelamento com a renderização do HTML e sua executação, após ser totalmente carregado.

`Defer`: Seu funcionamento é semelhante ao `Async`, porém a única diferença está na hora da execução do script, com o `Defer`, o script só será executado após o término de toda a análise do HTML. Ou seja, podemos ter uma tag `script` dentro da tag `head` selecionando um elemento no DOM, que não dará erro de **elemento não encontrado**, diferente do `Async` pois tem chances do `script` ser executado antes do **elemento ser analisado**.

### b) Para que serve o parâmetro &callback=initMap na url da api do google maps?
R: Pelo fato da tag script do google maps possuir os atributos `async/defer`, assim que o script executar e toda a API do Google Maps estiver carregada, ele executará o método `initMap`, que é responsável por setar o Mapa na página.

### c) O que acontece quando removemos o parâmetro &callback=initMap da url da api do google maps? Explique o porque.
R: Acontecerá que será carregada toda a API do Google Maps, porém o método principal para setar o Mapa na página não será executado, ocasionando numa página em branco.

### d) Descreva pelo menos uma forma de como podemos fazer com que a aplicação funcione corretamente mesmo sem este parâmetro.
R: Para resolver isso, tería que invocar o método diretamente no script da página, por exemplo no index.js, chamando o método assim: `initMap()`, além de adicionar o atributo `defer` na tag script caso o script esteja setado dentro da tag `<head>`, com isso esperará todo o HTML ser carregado e o elemento que é resgatado para setar o Mapa via JavaScript (`<div id="map"></div>`) estará disponível para utilização.

### e) Explique para que servem as seguintes tags do index.html: 
  `<link rel="manifest" href="manifest.json">
  <meta name="theme-color" content="">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">`

R:
`<link rel="manifest" href="manifest.json">`: é um JSON responsável por fornecer mini informações para celulares e até lojas de aplicativos. Por exemplo, mostrar alguma informação para o usuário enquanto os dados são carregados. Enfim, esse arquivo é obrigatório quando for criar uma PWA ( Progressive Web App ).

`<meta name="theme-color" content="">`: É uma meta responsável por pintar a barra de busca no seu celular por exemplo, suportada apenas no Google Chrome Android atualmente.

`<meta name="apple-mobile-web-app-capable" content="yes">`: Informa ao Safari que deve abrir o web app em tela cheia.

`<meta name="apple-mobile-web-app-status-bar-style" content="black">`: Informa ao iOS para pintar a barra de status do celular de preto.


### f) Está aplicação pode ser considerada um PWA? Em caso negativo, explique o que falta para que seja.
R: Não, pois o arquivo Manifest.json está setado no HTML porém não está na estrutura do projeto, e não possui outras tecnologias como Service Worker por exemplo.


## Angular Questão 4:

### a) Para que serve o método ngOnInit, quais são os outros métodos do Angular lifecycle hooks e para que servem?
R: o método ngOnInit é inicializado após o Angular mostrar todas as diretivas na página, ou seja, quando o componente for criado.

Os outros métodos são:
ngOnChanges()
ngDoCheck()
ngAfterContentInit()
ngAfterContentChecked()
ngAfterViewInit()
ngAfterViewChecked()
ngOnDestroy()

### b) Neste projeto, estamos usando os componentes gráficos da versão 4 da biblioteca gráfica do Ionic. Nesta versão, os componentes são Web Components. Explique o que são Web Components e explique quais são as vantagens deles.
R: Web Componentes são como se fossem pacotes empacatados utilizando as linguagens: HTML, CSS e JavaScript, ou seja, basta chamá-los na nossa página, por exemplo: <calendario></calendario> que irá mostrar um calendário com seu respectivo layout/estilo e suas funcionalidades.

As vantagens são que com Web Componentes conseguimos utilizar nosso componente em qualquer lib, exemplo: React, View. E Também ganhamos em organização de código, pois caso ocorra algum erro no componente, como está tudo empacotado dentro dele, você saberá onde tem que mudar, deixará mais facil as coisas.


### c) Para que serve a tag ngFor do angular?
R: O atributo ngFor serve para quando temos uma coleção de dados e queremos mostrar na tela essa coleção. Por exemplo uma coleção de nomes de pessoas e queremos mostrar dentro de cada <li> o nome correspondente, basta adicionar <li *ngFor="let nome of nomes">{{nome}}</li>.


### d) O que o codigo abaixo representa no arquivo list.page.ts?
`legends: Array<string> = []`
R: Ele representa uma coleção de dados vazia que é obrigatório cada dado ser uma string. No fim, dentro do componente, ele será responsável por mostrar os dados na tela.

### e) Como funciona a api Events do Ionic? Para que serve?
R: A API de Events serve para se comunicar com outros componentes.

### f) O que é flexbox? Para que servem as tags ion-grid, ion-row, ion-col? Quais as vantagens em utilizálas?
R: Flexbox Layout é biblioteca que está dentro do CSS, responsável por organizar elementos dentro de um container, deixando mais fácil a organização/posicionamento do mesmo. Essa biblioteca veio para facilitar a vida do desenvolvedor ao posicionar elementos, essa biblioteca é unidirecional, pois não foi feito para trabalhar com linhas e colunas ao mesmo tempo. Com isso, surgiu o CSS Grid, para controlarmos as linhas/colunas do nosso layout e na parte unidirecional o Flexbox, combinando perfeitamente.


## Angular Questão 6:

### a) Quais foram os problemas que você identificou?
R:

### b) Ordene os problemas por ordem de criticidade, ou seja, liste todos os problemas encontrados na ordem de quais deveriam ser corrigidos primeiro em um cenário onde devessemos priorizar as correções.
R:

### c) Justifique a ordem proposta no item anterior em termos de impacto para os usuários e dificuldade para corrigir o problema.
R:

### d) Para que servem os comandos async e await, encontrados na função presentLoading do arquivo home.page.ts?
R: Serve para transformar nosso código síncrono em assíncrono, porém não deixando sair da ordem de execução do síncrono.

### f) Quais as vantagens de utilizar async/await em códigos javascript/typescript?
R: A vantagem é que teremos um código assíncrono sendo executando de forma síncrona, deixará mais legível nosso código também.
