

// Google Maps Integration

var map = null;
var infoWindow = null;


const placesOfInterest = [
    { name: 'Charme da paulista', lat: -23.562172, lng: -46.655794 },
    { name: 'The Blue Pub', lat: -23.563112, lng: -46.650338 },
    { name: 'Veloso', lat: -23.585107, lng: -46.635219 },
    { name: 'Let\'s Beer', lat: -23.586508, lng: -46.641739 },
    { name: 'O\'Malley\'s', lat: -23.558296, lng: -46.665923 },
    { name: 'Finnegan\'s', lat: -23.559547, lng: -46.676794 },
    { name: 'Partisans', lat: -23.561049, lng: -46.682555 },
    { name: 'Morrison', lat: -23.555106, lng: -46.690883 },
    { name: 'Cão Véio', lat: -23.558130, lng: -46.679508 },
    { name: 'Cervejaria Nacional', lat: -23.564740, lng: -46.690641 },
    { name: 'Brewdog', lat: -23.561309, lng: -46.693935 },
    { name: 'Rei das Batidas', lat: -23.570613, lng: -46.705977 }
];

const detailsIconDefault = {
    path: 'M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z',
    fillColor: '#F7B217',
    fillOpacity: 0.98,
    scale: 0.98,
    strokeColor: '#666666',
    strokeWeight: 3
};

const arrMarkers = [];



function addMarker(marker, detailsIcon) {
    var marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(marker.lat, marker.lng),
        icon: detailsIcon,
        title: marker.name
    });

    marker.addListener('click', function() {
        changeColorMarker(marker, 'white');
        openInfoWindow(marker);
    });

    return arrMarkers.push(marker);
}



function changeColorMarker(marker, color) {
    resetColorMarkers(arrMarkers);

    var newDetails = Object.assign({}, detailsIconDefault);
    newDetails.fillColor = color;
    return marker.setIcon(newDetails);
}



function resetColorMarkers(markers) {
    if ( !isArray(markers) ) return false;
    arrMarkers.forEach( (marker) => marker.setIcon(detailsIconDefault) );
}



function openInfoWindow(marker) {
    if ( infoWindow ) infoWindow.close();

    infoWindow = new google.maps.InfoWindow({
        content: `<h2>${marker.title}</h2>`
    });

    infoWindow.open(map, marker);

    google.maps.event.addListener(infoWindow, 'closeclick', () =>
        resetColorMarkers(arrMarkers)
    );
}



function addCollectionMarker(collection) {
    if ( !isArray(collection) ) return false;
    collection.forEach( (marker) => addMarker(marker, detailsIconDefault) );
}



function isArray(arr) {
    return (
        Object.prototype.toString.call(arr) === '[object Array]'
        ? true : false
    );
}



/*
*   Create Map
*   Params: Object
*   Properties: lat, lng
*/

function createMap(params) {
    var mapOptions = {
        center: new google.maps.LatLng(params.lat, params.lng),
        gestureHandling: 'greedy',
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP]
        },
        disableDefaultUI: true,
        scaleControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
        }

    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    google.maps.event.addListener(map, 'idle', ResetBtnGeolocation);

}



function initMap() {
    createMap({lat: -23.562172, lng: -46.655794});
    addCollectionMarker(placesOfInterest);
    initGeolocation();
}



// Geolocation Feature

const btnGeolocation = document.querySelector('[data-geolocation="btn"]');


function initGeolocation() {

    // Validations
    if ( !btnGeolocation ) return false;
    if ( !navigator.geolocation ) return false;

    btnGeolocation.addEventListener('click', () => {
        navigator.geolocation.getCurrentPosition(locationSuccess, locationError)
    });

}



function locationSuccess(params) {
    var lat = params.coords.latitude;
    var lng = params.coords.longitude;

    var marker = { name: 'Minha Localização', lat, lng }

    btnGeolocation.textContent = 'Carregando...';
    btnGeolocation.classList.add('-loading');

    createMap({lat, lng});
    addMarker(marker, detailsIconDefault);
}



function locationError() {
    btnGeolocation.classList.add('-hide');
}



function ResetBtnGeolocation() {

    setTimeout( () => {
        btnGeolocation.textContent = 'Centralizar Mapa';
        btnGeolocation.classList.remove('-loading');
    }, 400);

}
